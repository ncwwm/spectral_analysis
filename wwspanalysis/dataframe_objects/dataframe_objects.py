import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
from wwspanalysis.dataframe_objects.array_objects import Spectrum, Array
from wwspanalysis.dataframe_objects.normalization_function import normalize_df
from wwspanalysis.dataframe_objects.figures import PairPlot, SpectralDfPlot, MeanSpectralDfPlot, HeatMap, HistPlot

pd.set_option("display.precision", 2)


class Dataframe:
    """
    A class to initiate a pandas dataframe from a .csv table, visualize the data, extract lines and columns and perform
    preprocessing
    """
    def __init__(self, path: str, index=None, index_col='index'):
        """
        Loading of the data from a .csv file
        :param path: the path of the file
        :param index: to limit the loading of certain index
        :param index_col: string containing the name of the index column in the .csv file
        """
        # loading of all the file
        self.df = pd.read_csv(path, index_col=index_col)
        if index is None:
            pass
        else:
            for i in index:
                assert (i in self.df.index), f'{i} is not a valid index'
            self.df = self.df.loc[index]

        self.columns = self.df.columns
        self.index = self.df.index

    def extract_line(self, index):
        """
        The line of the selected array is returned as a custom array object
        """
        assert index in self.index, 'This is not a valid index'
        return Array(str(index), self.df.loc[index].values)

    def extract_column(self, col_name):
        assert col_name in self.columns, 'This is not a valid column name'
        return Array(col_name, self.df[col_name].values)

    def normalize(self, method='zero_one', direction='line'):
        self.df = normalize_df(self.df, method=method, dimension=direction)

    def apply_savgol_filter(self, window_length=21, polyorder=2, deriv=0):
        """
        Application of a Savitszy-Golay filter to the data
        :param window_length: the length of the window
        :param polyorder: the order of the polynome to fit
        :param deriv: the degree of derivation
        """
        self.df = pd.DataFrame(savgol_filter(self.df, window_length, polyorder, deriv),
                               columns=self.columns, index=self.index)

    def apply_log(self):
        assert np.all(self.df > 0), 'Logarithm cannot be applied because the dataframe contains negative values'
        self.df = np.log(self.df)

    def statistics(self, precision=1):
        """
        Basic statistics about the dataframe
        """
        pd.set_option("display.precision", precision)
        return self.df.describe()

    def column_histplot(self, column_name: str, **kwargs):
        """
        Plotting a histogram of a column of the dataframe
        :param column_name:
        :param kwargs:
            Figure kwargs:
                figsize=(4, 4)
                title='Histogram'
            Seaborn histplot kwargs:
                kde=False
                bins=20
                stat='count'
                binwidth=None
                element='bars'
        """
        assert column_name in self.columns, 'This is not a valid column name'
        p = HistPlot(self.df, column_name=column_name, **kwargs)
        return p

    def correlation_heatmap(self, index=None, **kwargs):
        """
        Correlation heatmap between each column of the dataframe
        :param index:
        :param kwargs:
            General kwargs:
                title='Heatmap'
                figsize=(12, 10)
            Saeborn heatmap kwargs:
                annot=True
                square=True
                cmap='Reds'
                vmin=-0.5 minimum of the legend
                vmax=1 maximum of the legend
        """
        if index is None:
            index = self.df.index
        else:
            for i in index:
                assert i in self.index
        p = HeatMap(self.df.loc[index], **kwargs)
        return p


class SpectralDataFrame(Dataframe):

    def __init__(self, path, index=None, index_col='index'):
        """
        Loading of the data from a .csv file
        :param path: the path of the file
        :param index: to limit the loading of certain index
        :param index_col: string containing the name of the index column in the .csv file
        """
        super().__init__(path, index=index, index_col=index_col)
        self.wavelength = np.array([float(w) for w in self.df.columns])

    def extract_spectrum(self, index):
        """
        Similar to 'extract_line' but instead of returning a Array object, a Spectrum Array object is returned
        """
        assert index in self.index, 'This is not a valid index'
        return Spectrum(str(index), self.wavelength, self.df.loc[index].values)

    def spectral_df_plot(self, **kwargs):
        """
        Visualization of all the spectra in the dataframe
        Default kwargs:
            title='Spectral dataframe plot'
            figsize=(20, 12)
            cmap=current default colormap (sns.color_palette(sns.axes_style()['image.cmap']))
            linewidth=1
            linestyle='-'
            alpha_std=0.2
            xlabel='Wavelength [nm]'
            ylabel='Reflectance [-]'
        """
        p = SpectralDfPlot(self.df, **kwargs)
        return p

    def spectral_df_mean_plot(self, **kwargs):
        """
        Visualization of the average spectra of the dataframe
        Default kwargs
            title='Spectral dataframe plot'
            figsize=(20, 12)
            cmap=current default colormap (sns.color_palette(sns.axes_style()['image.cmap']))
            alpha=0.2
            xlabel='Wavelength [nm]'
            ylabel='Reflectance [-]'
        """
        p = MeanSpectralDfPlot(self.df, **kwargs)
        return p


class ParamDataFrame(Dataframe):
    """
    Dataframe containing pollution parameter
    """
    def __init__(self, path, index=None, index_col='index'):
        """
        Loading of the data from a .csv file
        :param path: the path of the file
        :param index: to limit the loading of certain index
        :param index_col: string containing the name of the index column in the .csv file
        """
        super().__init__(path, index=index, index_col=index_col)

    def visualisation_of_parameter_correlation(self, index=None, **kwargs):
        """
        Scatter plot of the different parameter to visually analyse the correlations
        :param index: to limit the loading of certain index
        Default kwargs:

        :return:
        """
        if index is None:
            index = self.df.index
        else:
            for i in index:
                assert i in self.index
        p = PairPlot(self.df.loc[index], **kwargs)
        return p



class LabelDataFrame(Dataframe):

    def __init__(self, path, index=None, index_col='index'):
        """
        Loading of the data from a .csv file
        :param path: the path of the file
        :param index: to limit the loading of certain index
        :param index_col: string containing the name of the index column in the .csv file
        """
        super().__init__(path, index=index, index_col=index_col)

    def get_group_index(self, col, grp_name):
        return self.df[self.df[col] == grp_name].index

    def get_group_index_as_dict(self, col):
        grp_list = list(set(self.df[col]))

        index_dict = {}

        for g in grp_list:
            index_dict[g] = self.df[self.df[col] == g].index

        return index_dict
