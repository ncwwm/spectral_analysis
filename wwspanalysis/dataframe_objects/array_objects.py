import numpy as np
import pandas as pd
from wwspanalysis.dataframe_objects.normalization_function.normalization import normalize_array
from wwspanalysis.dataframe_objects.figures import HistPlot
from wwspanalysis.general_figures.lineplot import LinePlot


class Array:
    """
    Custom array object with a name, and some specific functions (statistics, histplot, log, normalize)
    """
    def __init__(self, name: str, values: np.ndarray):
        """
        :param name: the name of the array (string)
        :param values: numpy array containing the values
        """
        assert isinstance(values, np.ndarray), 'Please provide a numpy array'
        self.name = str(name)
        self.values = values

    def normalize(self, method='std'):
        self.values = normalize_array(self.values, method=method)

    def apply_log(self):
        assert np.all(self.values > 0), 'Logarithm cannot be applied because the array contains negative values'
        self.values = np.log(self.values)

    def histplot(self, **hist_kwargs):
        """
        Plotting of an histogram of the array values
        Default kwargs:
            Figure: figsize=(10, 8), title='Histogram'
            Seaborn histplot: kde=False, bins=20, stat='count', binwidth=None, element='bars'
        """
        p = HistPlot(self.values, title=f'Histogramm of the {self.name}', **hist_kwargs)
        return p

    def statistics(self):
        """
        Basic statistics such as mean, median, ...
        """
        return pd.DataFrame(self.values).describe()


class Spectrum(Array):
    """
    In addition to a name and value, a Spectrum have a wavelength array corresponding to each values
    """
    def __init__(self, name: str, wavelength: np.ndarray, reflectance: np.ndarray):
        super().__init__(name, reflectance)
        assert isinstance(wavelength, np.ndarray), '\'Wavelength\' must be a numpy array'
        self.wavelength = wavelength

    def lineplot(self, **plot_kwargs):
        """
        Draw the spectrum
        Default  plot_kwargs:
            Figure: figsize=(20, 8), title='Plot of the spectrum'
            Seaborn lineplot: color, linewidth=2, linestyle='-'

        """
        plot_kwargs['title'] = plot_kwargs.get('title', f'Plot of spectrum nr {self.name}')
        p = LinePlot(self.wavelength, self.values, xlabel='Wavelength [nm]', ylabel='Reflectance [-]', **plot_kwargs)
        return p


class Parameter(Array):
    def __init__(self, sample: str, pollution: np.ndarray):
        super().__init__(sample, pollution)
