from wwspanalysis.general_figures import SNSFigure, SNSFigureKwargs
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


class MeanSpectralDfPlot(SNSFigure):
    """
    Plot of the mean spectra of a pandas dataframe containing multiple spectra
    """
    def __init__(self, data, wavelength_lst=None, index_dict=None, plot_std=True, **kwargs):

        # SNS figure kwargs
        fig_kwargs = SNSFigureKwargs(default_figsize=(4, 4), default_title='Mean spectra dataframe plot',
                                     default_xlabel='Wavelength [nm]', default_ylabel='Normalized reflectance [-]',
                                     **kwargs).__dict__

        # Other kwargs
        cmap = kwargs.get('cmap', sns.color_palette(sns.axes_style()['image.cmap']))
        linewidth = kwargs.get('linewidth', 1)
        linestyle = kwargs.get('linestyle', '-')
        alpha_std = kwargs.get('alpha_std', 0.2)

        # Initialization of the figure
        super().__init__(**fig_kwargs)

        # Preparation of the plot wavelength and labels
        if index_dict is None:
            index_dict = {'spectra': data.index.values}

        if wavelength_lst is None:
            wavelength_lst = np.array([float(w) for w in data.columns])
        else:
            for i in wavelength_lst:
                assert i in data.columns, 'This wavelength list is not valid'

        labels = list(index_dict.keys())

        # Plotting
        for i in range(len(labels)):
            m = data.loc[index_dict[labels[i]]].mean(axis=0)
            sns.lineplot(x=wavelength_lst, y=m,
                         color=cmap[i], linewidth=linewidth, linestyle=linestyle, label=labels[i])

            if plot_std:
                std = data.loc[index_dict[labels[i]]].std(axis=0)
                plt.fill_between(wavelength_lst, m-std/2, m+std/2, color=cmap[i], alpha=alpha_std)

        if index_dict is None:
            pass
        else:
            plt.legend()

        plt.close()


