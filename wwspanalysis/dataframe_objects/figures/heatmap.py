import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from wwspanalysis.general_figures.sns_figures import SNSFigure, SNSFigureKwargs


class HeatMap(SNSFigure):
    """
        Class to create a heatmap of the absolute correlation of the dataframe passed as parameter
        :param data: Dataframe to be plotted
        :param kwargs: Additional parameters for the figure and heatmap
    """
    def __init__(self, data: pd.DataFrame, **kwargs):

        # Extracting the arguments for the figure and the plot (seaborn Heatmap)
        fig_kwargs = SNSFigureKwargs(default_figsize=(6, 5), default_title='Histplot', **kwargs).__dict__
        heatmap_kwargs = HeatMapKwargs(**kwargs).__dict__

        # Initiating a figure
        super().__init__(**fig_kwargs)

        # Plotting the heatmap
        corr = np.abs(data.corr())
        mask = np.triu(np.ones(corr.shape)).astype(np.bool_) # Masking the upper half of the values to avoid repetition
        sns.heatmap(corr, mask=mask, cbar_kws={'label': 'pearson correlation (absolute)'}, **heatmap_kwargs)

        # Rotating the ticks
        plt.yticks(rotation=0)
        plt.xticks(rotation=90)

        # Closing the plot
        plt.close()


class HeatMapKwargs:
    """
        Class to store the additional parameters for the heatmap
    """
    def __init__(self, **kwargs):
        self.annot = kwargs.get('annot', True)
        self.square = kwargs.get('square', True)
        self.cmap = kwargs.get('cmap', 'Reds')
        self.vmax = kwargs.get('vmax', 1)
        self.vmin = kwargs.get('vmin', 0)