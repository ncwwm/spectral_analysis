from IPython.display import display
from matplotlib import pyplot as plt
from datetime import datetime
import seaborn as sns
import os
from wwspanalysis.general_figures.sns_style import SeabornTheme


class PairPlot():
    def __init__(self, data, **kwargs):

        sns.set_theme(**SeabornTheme().__dict__)

        title = kwargs.get('title', 'Correlation matrix plot')
        kws = PairplotKwargs(**kwargs).__dict__

        self.fig = sns.pairplot(data, **kws)
        self.fig.figure.suptitle(title, fontsize=sns.plotting_context().get('axes.titlesize'))
        plt.close()

    def show(self, save_fig=False, save_path=None, file_format='png'):
        display(self.fig.figure)
        if save_fig:
            self.save(save_path, file_format)

    def save(self, save_path, file_format='png', overwrite=False):
        if save_path is None:
            save_path = datetime.now().strftime('%Y_%m_%d_%H_h_%M_min')+'_saved_figure'
        if not overwrite:
            save_path = unique_file_name(save_path, file_format)
        self.fig.savefig(save_path)


class PairplotKwargs:
    def __init__(self, **kwargs):
        self.height = kwargs.get('height', 2)
        self.corner = kwargs.get('corner', True)

def unique_file_name(file_name, file_format):
    i = 1
    while os.path.exists(f"{file_name}_{i}.{file_format}"):
        i += 1
    return f"{file_name}_{i}.{file_format}"