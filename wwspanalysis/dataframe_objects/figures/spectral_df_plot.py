from wwspanalysis.general_figures.sns_figures import SNSFigure, SNSFigureKwargs
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


class SpectralDfPlot(SNSFigure):
    def __init__(self, data, wavelength_lst=None, index_dict=None, plot_mean=False, plot_legend=True, **kwargs):

        # SNS figure kwargs
        fig_kwargs = SNSFigureKwargs(default_figsize=(4, 4), default_title='Mean spectra dataframe plot',
                                     default_xlabel='Wavelength [nm]', default_ylabel='Normalized reflectance [-]',
                                     **kwargs).__dict__

        # Other kwargs
        cmap = kwargs.get('cmap', sns.color_palette(sns.axes_style()['image.cmap']))
        alpha = kwargs.get('alpha', 0.2)
        linestyle = kwargs.get('linestyle', '-')

        # Initialization of the figure
        super().__init__(**fig_kwargs)

        # Preparation of the plot wavelength and labels
        if index_dict is None:
            index_dict = {'spectra': data.index.values}

        if wavelength_lst is None:
            wavelength_lst = np.array([float(w) for w in data.columns])

        labels = list(index_dict.keys())

        if len(linestyle) != len(labels):
            if len(linestyle) == 1:
                linestyle = [linestyle[0] for i in range(len(labels))]
            else:
                raise ValueError('The linestyle argument must be either a list with one element or of the size of index_dict')


        # Plotting
        for i in range(len(labels)):
            for ind in index_dict[labels[i]]:
                if ind == index_dict[labels[i]][-1]:
                    sns.lineplot(x=wavelength_lst, y=data.loc[ind].values, alpha=alpha, color=cmap[i], label=labels[i],
                                 ls=linestyle[i],  legend=False)
                else:
                    sns.lineplot(x=wavelength_lst, y=data.loc[ind].values, alpha=alpha, color=cmap[i],
                                 ls=linestyle[i], legend=False)

            if plot_mean:
                sns.lineplot(x=wavelength_lst, y=data.loc[index_dict[labels[i]]].mean(axis=0), ls='--', color='black',
                             linewidth=2, label='average')

        if plot_legend:
            plt.legend()

        plt.close()
