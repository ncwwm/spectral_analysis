import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from wwspanalysis.general_figures.sns_figures import SNSFigure, SNSFigureKwargs


class HistPlot(SNSFigure):
    def __init__(self, data, column_name=None, **kwargs):
        """
        :param data: an array or a pandas dataframe
        :param column_name: if data is a pandas dataframe, name f the column to generate the histplot
        """

        # Figure title

        if column_name:
            default_title = f'Histogram of the {column_name} values'
        else:
            default_title = 'Histogramm'

        # Figure kwargs
        fig_kwargs = SNSFigureKwargs(default_figsize=(4, 4), default_title=default_title, **kwargs).__dict__
        histplot_kwargs = HistplotKwargs(**kwargs).__dict__

        #Initialization of the figure
        super().__init__(**fig_kwargs)

        # Drawing of the histplot in the figure

        if isinstance(data, np.ndarray):
            column_name = None

        sns.histplot(data, x=column_name, **histplot_kwargs)

        plt.close()


class HistplotKwargs:
    # See seaborn documentation
    def __init__(self, **kwargs):
        self.kde = kwargs.get('kde', False)
        self.bins = kwargs.get('bins', 20)
        self.stat = kwargs.get('stat', 'count')
        self.binwidth = kwargs.get('binwidth')
        self.element = kwargs.get('element', 'bars')
