import pandas as pd
import numpy as np


def normalize_array(arr: np.ndarray, method='std'):
    assert method in ['mean', 'std', 'zero_one'], 'This normalization method is not available'

    if method == 'mean':  # Mean normalization
        arr = (arr - arr.mean()) / (arr.max() - arr.min())

    elif method == 'zero_one':  # Scale between zero and one
        arr = (arr - arr.min()) / (arr.max() - arr.min())

    elif method == 'std':  # StandardScaler
        arr = (arr - arr.mean()) / (arr.std())

    return arr


def normalize_df(df: pd.DataFrame, method='std', dimension='line'):
    assert method in ['mean', 'std', 'zero_one'], 'This normalization method is not available'
    assert dimension in ['line', 'col'], 'The normalization dimension is either \'line\' or \'col\''

    val = df.values.copy()

    if dimension == 'col':
        val = val.T

    for i in range(len(val)):
        val[i] = normalize_array(val[i], method=method)  # normalization of the arrays one by one

    if dimension == 'col':
        val = val.T

    norm_df = pd.DataFrame(data=val, index=df.index, columns=df.columns)

    return norm_df


def normalize_df(df: pd.DataFrame, method='std', dimension='line'):
    assert method in ['mean', 'std', 'zero_one'], 'This normalization method is not available'
    assert dimension in ['line', 'col'], 'The normalization dimension is either \'line\' or \'col\''

    val = df.values.copy()

    if dimension == 'col':
        val = val.T

    for i in range(len(val)):
        val[i] = normalize_array(val[i], method=method)  # normalization of the arrays one by one

    if dimension == 'col':
        val = val.T

    norm_df = pd.DataFrame(data=val, index=df.index, columns=df.columns)

    return norm_df