import matplotlib.pyplot as plt
from IPython.display import display
from datetime import datetime
import seaborn as sns
import os
from wwspanalysis.general_figures.sns_style import SeabornTheme


class SNSFigure:
    def __init__(self, **fig_kwargs):
        sns.set_theme(**SeabornTheme().__dict__)

        kws = SNSFigureKwargs(**fig_kwargs)

        self.fig = plt.figure(figsize=kws.figsize, tight_layout=kws.tight_layout)
        plt.suptitle(kws.title)
        plt.xlabel(kws.xlabel)
        plt.ylabel(kws.ylabel)
        plt.xscale(kws.xscale)
        plt.yscale(kws.yscale)

    def show(self, save_fig=False, save_path=None, file_format='png'):
        display(self.fig)
        if save_fig:
            self.save(save_path, file_format)

    def save(self, save_path, file_format='png', overwrite=False):
        if save_path is None:
            save_path = datetime.now().strftime('%Y_%m_%d_%H_h_%M_min')+'_saved_figure'
        if not overwrite:
            save_path = unique_file_name(save_path, file_format)
        self.fig.savefig(save_path)


class SNSFigureKwargs:
    def __init__(self, default_figsize=(6, 4), default_title='My amazing figure',
                 default_xlabel=None, default_ylabel=None, default_xscale='linear', default_yscale='linear', **kwargs):
        self.figsize = kwargs.get('figsize', default_figsize)
        self.tight_layout = kwargs.get('tight_layout', True)
        self.title = kwargs.get('title', default_title)
        self.xlabel = kwargs.get('xlabel', default_xlabel)
        self.ylabel = kwargs.get('ylabel', default_ylabel)
        self.xscale = kwargs.get('xscale', default_xscale)
        self.yscale = kwargs.get('yscale', default_yscale)


def unique_file_name(file_name, file_format):
    i = 1
    while os.path.exists(f"{file_name}_{i}.{file_format}"):
        i += 1
    return f"{file_name}_{i}.{file_format}"
