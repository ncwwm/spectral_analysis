class SeabornTheme:
    def __init__(self, **kwargs):
        self.context = kwargs.get('context', 'notebook')
        self.style = kwargs.get('style', 'white')
        self.palette = kwargs.get('palette', 'tab10')
        self.font_scale = kwargs.get('font_scale', 1.5)
        self.rc = kwargs.get('rc', {'axes.titlesize': 15.0, 'image.cmap': 'tab10', 'xtick.bottom': True, 'ytick.left': True, 'axes.grid': True, 'figure.edgecolor':'black'})
