from wwspanalysis.general_figures.sns_figures import SNSFigure, SNSFigureKwargs
import seaborn as sns
import matplotlib.pyplot as plt

class LinePlot(SNSFigure):
    def __init__(self, x, y, xlim=None, ylim=None, **kwargs):
        """
        :param x: array
        :param y: array
        """

        # Getting the  and lineplot kwargs
        fig_kws = SNSFigureKwargs(default_figsize=(6, 4), default_title='Lineplot', **kwargs).__dict__
        lineplot_kws = LinePlotKwargs(**kwargs).__dict__

        # initialization of the figure
        super().__init__(**fig_kws)

        # drawing of the lineplot
        sns.lineplot(x=x, y=y, **lineplot_kws)

        # Setting the x and y limits
        plt.xlim(xlim)
        plt.ylim(ylim)

        plt.close()

class LinePlotKwargs:
    # see Seaborn documentation
    def __init__(self, **kwargs):
        self.color = kwargs.get('color', sns.color_palette(sns.axes_style()['image.cmap'])[0])
        self.linewidth = kwargs.get('linewidth', 2)
        self.linestyle = kwargs.get('linestyle', 'solid')
