from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, r2_score
from wwspanalysis.PLS_analysis.pls_figures.pls_figures import RegressionAnalysisPlot, RegressionAnalysisPlotDetailed
import math


class PLSmodel:
    """
    Custom class initiated with the training of a PLS model with the given parameters,
    with methods to evaluate and visualize the model performances.
    """
    def __init__(self, x, y, n_components=5, cv=None):
        """
        param:: x: the features
        param:: y: labels
        param:: n_components: number of components of the PLS model (default 5)
        param:: cv: number of cross validation sub-sets (default: len(y) (loocv))
        """

        if cv is None:
            cv = len(y)

        self.model = PLSRegression(n_components=n_components)
        self.model.fit(x, y)
        self.y_calib = self.model.predict(x)[:, 0]
        self.y_cv = cross_val_predict(self.model, x, y, cv=cv)[:, 0]
        self.__y = y
        self.__x = x
        self.__n_components = n_components

    def evaluate(self):
        """
        Return a dictionary with different model performance parameters"
        """
        return {
            'r2_calib': r2_score(self.__y, self.y_calib),
            'r2_cv': r2_score(self.__y, self.y_cv),
            'mape_calib': mean_absolute_percentage_error(self.__y, self.y_calib),
            'mape_cv': mean_absolute_percentage_error(self.__y, self.y_cv),
            'mae_calib': mean_absolute_error(self.__y, self.y_calib),
            'mae_cv': mean_absolute_error(self.__y, self.y_cv),
            'rmse_calib': math.sqrt(mean_squared_error(self.__y, self.y_calib)),
            'rmse_cv': math.sqrt(mean_squared_error(self.__y, self.y_cv)),
            'rel_rmse_calib': math.sqrt(mean_squared_error(self.__y, self.y_calib)) / self.__y.mean(),
            'rel_rmse_cv': math.sqrt(mean_squared_error(self.__y, self.y_cv)) / self.__y.mean()
        }

    def visualize(self, **kwargs):
        """
        Plotting of the regression results
        """
        v = RegressionAnalysisPlot(self.__y, self.y_cv, **kwargs)
        return v

    def visualize_detailed(self, **kwargs):
        """
        Plotting of the regression results with residuals
        """
        v = RegressionAnalysisPlotDetailed(self.__y, self.y_cv, **kwargs)
        return v

    def summary(self):
        """
        Nicely formatted overviey of the PLS performances
        """
        eval = self.evaluate()
        print('Number of comporents for the PLS: %s' % self.__n_components)
        print('Number of wavelength for the PLS: %s' % len(self.__y))
        print('R2 \n  Calibration: %5.3f \n  Cross validation: %5.3f' % (eval["r2_calib"], eval["r2_cv"]))
        print('RMSE \n  Calibration: %5.3f \n  Cross validation: %5.3f' % (eval["rmse_calib"], eval["rmse_cv"]))
        print('relative RMSE \n  Calibration: %5.3f \n  Cross validation: %5.3f' % (eval["rel_rmse_calib"], eval["rel_rmse_cv"]))



