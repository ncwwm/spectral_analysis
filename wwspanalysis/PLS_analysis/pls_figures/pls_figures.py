from wwspanalysis.general_figures import SNSFigure, SNSFigureKwargs, SeabornTheme
from sklearn.metrics import mean_squared_error, r2_score
from datetime import datetime
import seaborn as sns
import math
import matplotlib.pyplot as plt
import os


class RegressionAnalysisPlotDetailed():
    def __init__(self, y, y_predict, **kwargs):

        sns.set_theme(**SeabornTheme().__dict__)

        title = kwargs.get('title', 'PLS regression')
        figsize = kwargs.get('figsize', (10, 4))

        self.fig, self.ax = plt.subplots(1, 3, figsize=figsize)

        r2_cv = r2_score(y, y_predict)
        mse_calib = mean_squared_error(y, y_predict)

        txt = f'R\u00B2={int(1000 * r2_cv) / 1000}\nRMSE={int(10 * math.sqrt(mse_calib)) / 10}'

        sns.scatterplot(ax=self.ax[0], x=y, y=y_predict).set(title='Regression results')

        m = max(max(y), max(y_predict))
        self.ax[0].plot([0, m], [0, m], ls='--', color='r')
        self.ax[0].set_xlabel('Reference value')
        self.ax[0].set_ylabel('Prediction')

        self.ax[0].text(0.01 * m, 0.99 * m, txt, verticalalignment='top', bbox=dict(facecolor='white', alpha=1, edgecolor='black'))

        error = y_predict-y
        sns.scatterplot(ax=self.ax[1], x=y, y=error).set(title='Residuals')
        self.ax[1].set_xlabel('Reference value')
        self.ax[1].set_ylabel('Error')

        sns.histplot(ax=self.ax[2], x=error, bins=20).set(title='Residuals histogram')
        self.ax[2].set_xlabel('Error')

        m2 = max(abs(min(error)), abs(max(error)))
        self.ax[2].set_xlim([-m2, m2])

        plt.suptitle(title, fontsize=18)
        plt.tight_layout()
        plt.close()

    def show(self, save_fig=False, save_path=None, file_format='png'):
        display(self.fig)
        if save_fig:
            self.save(save_path, file_format)

    def save(self, save_path, file_format='png', overwrite=False):
        if save_path is None:
            save_path = datetime.now().strftime('%Y_%m_%d_%H_h_%M_min')+'_saved_figure'
        if not overwrite:
            save_path = unique_file_name(save_path, file_format)
        self.fig.savefig(save_path)

def unique_file_name(file_name, file_format):
    i = 1
    while os.path.exists(f"{file_name}_{i}.{file_format}"):
        i += 1
    return f"{file_name}_{i}.{file_format}"


class RegressionAnalysisPlot(SNSFigure):
    def __init__(self, y, y_predict, **kwargs):

        fig_kwargs = SNSFigureKwargs(default_title='PLS regression', default_xlabel='Reference value',
                                     default_ylabel='Prediction', default_figsize=(4, 4), **kwargs).__dict__


        super().__init__(**fig_kwargs)

        r2_cv = r2_score(y, y_predict)
        mse_calib = mean_squared_error(y, y_predict)

        txt = f'R\u00B2={int(1000 * r2_cv) / 1000}\nRMSE={int(10 * math.sqrt(mse_calib)) / 10}'

        sns.scatterplot(x=y, y=y_predict)

        m = max(max(y), max(y_predict))
        plt.plot([0, m], [0, m], ls='--', color='r')

        plt.text(0.01 * m, 0.99 * m, txt, verticalalignment='top', bbox=dict(facecolor='white', alpha=1, edgecolor='black'))

        plt.close()