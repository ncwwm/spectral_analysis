from wwspanalysis.PLS_analysis.PLSmodel import PLSmodel
from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
import math


class PLSOptimizer:
    def __init__(self, x, y, n_max=20, cv=5, wavelength_selection=True):

        self.__x = x
        self.__y = y

        s = x.shape

        self.mse = np.zeros([n_max, s[1]])
        ind = []

        for n_c in range(1, n_max + 1):

            if wavelength_selection:

                # Analysing the wavelegnth importance with a first PLS model on the whole dataset
                pls = PLSRegression(n_components=n_c)
                pls.fit(x, y)

                sorted_ind = np.argsort(np.abs(pls.coef_[:, 0]))  # ordering the indexes from low to high regression coeff
                ind.append(sorted_ind)  # saving the order for this number of compoments

                # Discarding the wavelength one by one and cross-val testing a model
                for w in range(0, len(sorted_ind) + 1 - n_c):
                    x2 = x[:, sorted_ind[w:]]
                    pls2 = PLSRegression(n_components=n_c)
                    y_cv = cross_val_predict(pls2, x2, y, cv=cv)  ########## CHANGE ##########

                    # saving the mse of this particular combination of wavelength and n_components
                    self.mse[n_c - 1, w] = mean_squared_error(y_cv, y)

                    # extracting the best combinatio of parameters

            else:
                pls = PLSRegression(n_components=n_c)
                y_cv = cross_val_predict(pls, x, y, cv=cv)
                self.mse[n_c - 1, 0] = mean_squared_error(y_cv, y)

        self.n_opti, n_wav = np.where(self.mse == np.min(self.mse[np.nonzero(self.mse)]))
        self.rmse_opti = math.sqrt(self.mse[self.n_opti, n_wav])
        self.n_opti = self.n_opti[0] + 1
        n_wav = n_wav[0]
        if not ind:
            self.wav_opti = [i for i in range(s[1])]
        else:
            self.wav_opti = ind[self.n_opti - 1][n_wav:]


    def optimal_model(self, cv=None):
        return PLSmodel(self.__x[:, self.wav_opti], self.__y, n_components=self.n_opti, cv=cv)

    def summary(self):
        print('Number of comporents for the PLS: %s' % self.n_opti)
        print('Number of wavelength for the PLS: %s' % len(self.wav_opti))
        print('Optimal RMSE:', self.rmse_opti)

    def visualize_optimization(self):
        plt.figure(figsize=(12, 10))

        for i in range(len(self.mse)):
            plt.plot(self.mse[i, :280 - i], label=i + 1)

        plt.legend()
        plt.show()

    def plot_optimal_wavelength(self):
        wavelength = [400 + 2 * i for i in range(300)]
        w_select = self.wav_opti * 2 + 400
        t = []

        for i in range(len(wavelength)):
            if wavelength[i] in w_select:
                t.append((wavelength[i] - 1, 2))

        fig, ax = plt.subplots(figsize=(20, 8))
        ax.broken_barh(t, (0, 1), facecolors='red', edgecolors='red', alpha=1)
        plt.show()
