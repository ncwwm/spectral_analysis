from setuptools import setup, find_packages

setup(name='wwspanalysis',
      version='1.0.1',
      description='Module to preprocess spectral data and use machine learning tools for pollution modelling',
      classifiers=[
        'Development Status :: early stage',
        'Programming Language :: Python :: 3',
      ],
      install_requires=['numpy==1.24.1', 'pandas==1.5.3', 'scipy==1.10.0', 'matplotlib==3.6.3', 'scikit-learn==1.2.0', 'seaborn==0.12.2'],
      packages=find_packages(exclude=("doc",".git",".idea","venv", "test")),
      keywords='Water Management',
      author='Pierre Lechevallier',
      author_email='pierre.lechevallier@eawag.ch',
      license='MIT')