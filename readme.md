# Preprocessing and modelling spectral data to estimate wastewater pollution

This package provides three classes to load and preprocess experimental data:
* `SpectralDataFrame`
* `ParamDataFrame`
* `LabelDataFrame`

In addition, two classes are provided to use partial least square for data-driven modelling:
* `PLSmodel`
* `PLSoptimizer`


This package has been created for a custom processing of data collected for research purpose. 

## Installation

To install the package, run the following command:


```shell
git clone https://gitlab.switch.ch/ncwwm/spectral_analysis.git
```

```shell
pip install wwspanalysis
```

## Description and usage examples

### Dataframe classes to load, visualize and preprocess data

* `SpectralDataFrame` class:

This class makes it possible to open a .csv file containing spectra. The .csv file should have the following structure:

| index | wavelength1 (number) | wavelength2 (number) | ... |
|-------|----------------------|----------------------|-----|
| 0     | XXX                  | XXX                  | ... |
| 1     | XXX                  | XXX                  | ... |
| ...   | ...                  | ...                  | ... |

Once initiated, the DataFrame can be pre-processed (normalization, SG filter), lines and columns can be extracted and different visualization can be made.

Example of use
```python
# Loading the module
from wwspanalysis import SpectralDataFrame

# Initiating a spectral dataframe
path = 'folder_path/spectra.csv'
sp = SpectralDataFrame(path=path)

# pre-processing the spectra with SG filter
sp.apply_savgol_filter(window_length=23, polyorder=3, deriv=2)

# displaying the spectra
sp.spectral_df_plot().show()

# extracting the 15th spectrum
spectrum = sp.extract_spectrum(index=15)
print(spectrum.values)
```
* `ParamDataFrame` class:
  
This class makes it possible to open a .csv file containing pollution data. The .csv file should have the following structure:

| index | variable1 name | variable2 name | ... |
|-------|----------------|----------------|-----|
| 0     | XXX            | XXX            | ... |
| 1     | XXX            | XXX            | ... |
| ...   | ...            | ...            | ... |

Similar methods as the `SpectralDataFrame` class are available

Example of use
```python
# Loading the modula
from wwspanalysis import ParamDataFrame

# Initiating a spectral dataframe
path = 'folder_path/param.csv'
param = ParamDataFrame(path=path)

# Visualizing the different correlations
param.visualisation_of_parameter_correlation().show()

# Visualizing the correlation heatmap
param.correlation_heatmap().show()

# Printing some dataframe statistics:
param.statistics()


```
* `LabelDataFrame` class:

This class makes it possible to open a .scv file containing metadata for each sample. The .csv file should have the following structure:

| index | info1 | info2 | ... |
|-------|-------|-------|-----|
| 0     | XXX   | XXX   | ... |
| 1     | XXX   | XXX   | ... |
| ...   | ...   | ...   | ... |


Example of use

```python
# Loading the module
from wwspanalysis import LabelDataFrame

# Initiating a spectral dataframe
path = 'folder_path/label.csv'
label = LabelDataFrame(path=path)

# Extracting the indices of all the samples part of the group1 stored in columns1 of the dataframe
indices = lb.get_group_index(col='column1', grp_name='group1')

# Extracting the different groups and their indices in column1 as a dictionary
indices_dict = lb.get_group_index_as_dict(col='column1')
```
Example of use in combination with the other dataframe objects

```python
# Loading the module
from wwspanalysis import LabelDataFrame, SpectralDataFrame, ParamDataFrame

# Initiating a spectral dataframe
path_label = 'folder_path/label.csv'
lb = LabelDataFrame(path=path_label)

# Extracting the indices of all the samples part of the group1 stored in columns1 of the dataframe
indices = lb.get_group_index(col='column1', grp_name='group1')

# Loading the data corresponding to the indices
path_spectr = 'folder_path/spectra.csv'
spectr = SpectralDataFrame(path=path_spectr, index=indices)

path_param = 'folder_path/param.csv'
param = ParamDataFrame(path=path_param, index=indices)
```

### Two classes are available to group indices depending on the information contained in the dataframe.


* `PLSmodel` class:

A PLS model trained and cross-validates with the given parameters. Different methods enables to visualize and get the model performances.

```python
# Loading the module
from wwspanalysis import SpectralDataFrame, ParamDataFrame, PLSmodel

# Loading the data. Note: make sure the indices in the spectral data and the pollution data are matching
sp = SpectralDataFrame(path='folder_path/spectra.csv')
pa = ParamDataFrame(path='folder_path/pollution.csv')

# Extracting the necessary values for the training of the model
x = sp.df.values  # features: spectra
y = pa.extract_column('COD').values  # labels: COD values

# Training a PLS model with 12 components. The model will be cross-validated with 4sub-section of the data.
model = PLSmodel(x, y, n_components=12, cv=4)

# Visualization of the results
model.visualize_detailed().show()

# Getting the model performances as a dictionary
result_dict = model.evaluate()

# Displaying the model performances
model.summary()
```
* `PLSmoptimizer` class:

During the initialization, the optimal combination of number of PLS components and number of features (stepwise removal of the features with low PLS coefficient) will be searched.


```python
# Loading the module
from wwspanalysis import SpectralDataFrame, ParamDataFrame, PLSOptimizer

# Loading the data. Note: make sure the indices in the spectral data and the pollution data are matching
sp = SpectralDataFrame(path='folder_path/spectra.csv')
pa = ParamDataFrame(path='folder_path/pollution.csv')

# Extracting the necessary values for the training of the model
x = sp.df.values  # features: spectra
y = pa.extract_column('COD').values  # labels: COD values

# Optimizing the number of components (max. 10) and the number of wavelength 
op = PLSOptimizer(x, y, n_max=10, wavelength_selection=True)

# Visualizing the optimization process
op.visualize_optimization()

# Analyzing the optimal model
optimal_model = op.optimal_model()
optimal_model.visualize_detailed().show()

# Visualazing the selected wavelength
op.plot_optimal_wavelength()
```

## Using devellopped tests

First you need to install `pytest`, which is listed in the developper requirements

```shell
pip install pytest
```

Running the tests:
```shell
pytest
```


## Dependencies

This package depends on the following python packages:
* `numpy==1.24.1`
* `pandas==1.5.3`
* `scipy==1.10.0`
* `matplotlib==3.6.3`
* `scikit-learn==1.2.0`
* `seaborn==0.12.2`