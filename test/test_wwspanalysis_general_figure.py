from wwspanalysis.general_figures import SNSFigure, LinePlot
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def test_SNSFigure():
    # Create an instance of the SNSFigure class
    fig = SNSFigure(title='Test Title', xlabel='Test X Label', ylabel='Test Y Label', xscale='linear', yscale='linear')

    # Assert that the object is an instance of SNSFigure
    assert isinstance(fig, SNSFigure)
    assert isinstance(fig.fig, plt.Figure)
    assert fig.fig._suptitle.get_text() == 'Test Title'

def test_LinePlot():
    x = np.linspace(0, 10, num=100)
    y = np.sin(x)

    lineplot = LinePlot(x, y)
    assert lineplot.fig is not None
    assert sns.lineplot is not None
