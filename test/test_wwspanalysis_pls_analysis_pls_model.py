from wwspanalysis import PLSmodel


def test_pls_model(data_for_ml):
    c = data_for_ml.columns
    features = data_for_ml[c[:-1]]
    labels = data_for_ml[c[-1]]

    m = PLSmodel(features, labels, n_components=2)

    assert len(m.y_cv)==len(labels)

