import numpy as np
import pandas as pd
import pytest
from wwspanalysis.dataframe_objects.array_objects import Array
from wwspanalysis.dataframe_objects.figures import HistPlot

class TestArray:
    def test_init(self):
        # Test that the class is initialized correctly
        values = np.random.rand(5)
        array = Array('test_array', values)
        assert array.name == 'test_array'
        assert np.all(array.values == values)
        with pytest.raises(AssertionError):
            array = Array('test_array', 'not_an_array')

    def test_normalize(self):
        # Test that the normalize method works correctly
        values = np.random.rand(5)
        array = Array('test_array', values)
        array.normalize()
        assert np.allclose(np.mean(array.values), 0)
        assert np.allclose(np.std(array.values), 1)

    def test_apply_log(self):
        # Test that the apply_log method works correctly
        values = np.random.rand(5)+1
        array = Array('test_array', values)
        array.apply_log()
        assert np.all(np.log(values) == array.values)

    def test_histplot(self):
        # Test that the histplot method works correctly
        values = np.random.rand(5)
        array = Array('test_array', values)
        plot = array.histplot()
        assert isinstance(plot, HistPlot)

    def test_statistics(self):
        # Test that the statistics method works correctly
        values = np.array([i-50 for i in range(101)])
        array = Array('test_array', values)
        stats = array.statistics()
        assert isinstance(stats, pd.DataFrame)
        assert pytest.approx(stats.loc['mean'], 0.1) == 0