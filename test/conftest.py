import pandas as pd
import numpy as np
import pytest


@pytest.fixture
def data_for_ml():
    a = np.array([i for i in range(20)])
    b = np.random.rand(20)
    c = np.random.rand(20)
    d = np.random.rand(20)
    e = np.random.rand(20)
    f = np.random.rand(20)

    data = pd.DataFrame({'100': a, '200': b, '300': c, '400': c, '500': e, '600': f, 'labels':3*a+7*b})

    return data
