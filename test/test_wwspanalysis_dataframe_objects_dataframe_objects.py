from wwspanalysis.dataframe_objects.dataframe_objects import Dataframe
from wwspanalysis.dataframe_objects.array_objects import Array, Spectrum
from wwspanalysis import SpectralDataFrame, LabelDataFrame, ParamDataFrame
import pandas as pd
import numpy as np
import pytest
from pathlib import Path
import os

class TestDataframe:
    @pytest.fixture
    def dataframe(self):
        dir_path = os.path.dirname(__file__)
        path = dir_path+'/test_data/reflectance_spectra.csv'
        assert Path(path).exists(), f'{path} does not exist'
        return Dataframe(path)

    def test_init(self, dataframe):
        assert isinstance(dataframe.df, pd.DataFrame)
        assert np.all(dataframe.columns == dataframe.df.columns)
        assert np.all(dataframe.index == dataframe.df.index)

    def test_extract_line(self, dataframe):
        index = 0
        assert index in dataframe.index, 'This is not a valid index'
        line = dataframe.extract_line(index)
        assert isinstance(line, Array)
        assert line.name == str(index)
        assert np.array_equal(line.values, dataframe.df.loc[index].values)

    def test_extract_column(self, dataframe):
        col_name = dataframe.columns[0]
        column = dataframe.extract_column(col_name)
        assert isinstance(column, Array)
        assert column.name == col_name
        assert np.array_equal(column.values, dataframe.df[col_name].values)

class TestSpectralDataFrame:
    @pytest.fixture
    def dataframe(self):
        dir_path = os.path.dirname(__file__)
        path = dir_path+'/test_data/reflectance_spectra.csv'
        assert Path(path).exists(), f'{path} does not exist'
        return SpectralDataFrame(path)

    def test_init(self, dataframe):
        assert isinstance(dataframe, Dataframe)
        assert isinstance(dataframe.df, pd.DataFrame)
        assert np.all(dataframe.wavelength == np.array([float(w) for w in dataframe.columns]))

    def test_extract_spectrum(self, dataframe):
        index = 0
        assert index in dataframe.index, 'This is not a valid index'
        sp = dataframe.extract_spectrum(index)
        assert isinstance(sp, Spectrum)
        assert sp.name == str(index)
        assert np.all(sp.wavelength == dataframe.wavelength)
        assert np.array_equal(sp.values, dataframe.df.loc[index].values)

class TestParamDataFrame:
    @pytest.fixture
    def dataframe(self):
        dir_path = os.path.dirname(__file__)
        path = dir_path+'/test_data/pollution_data.csv'
        assert Path(path).exists(), f'{path} does not exist'
        return ParamDataFrame(path)

    def test_init(self, dataframe):
        assert isinstance(dataframe, Dataframe)
        assert isinstance(dataframe.df, pd.DataFrame)

class TestLabelDataFrame:
    @pytest.fixture
    def dataframe(self):
        dir_path = os.path.dirname(__file__)
        path = dir_path+'/test_data/information_and_labels.csv'
        assert Path(path).exists(), f'{path} does not exist'
        return LabelDataFrame(path)

    def test_init(self, dataframe):
        assert isinstance(dataframe, Dataframe)
        assert isinstance(dataframe.df, pd.DataFrame)

    def test_get_group_index(self, dataframe):
        i = dataframe.get_group_index('group', 'only_ww')
        assert len(i) == 1
        assert i[0] == 0

        i2 = dataframe.get_group_index('group', 'with_forma')
        assert np.all(i2 == [1, 2, 3])

    def test_get_group_index_as_dict(self, dataframe):
        d = dataframe.get_group_index_as_dict('group')

        assert isinstance(d, dict)
        assert len(list(d.keys())) == 2
        assert 'only_ww' in d.keys()
        assert 'with_forma' in d.keys()
        assert np.all(d['only_ww'] == [0])
        assert np.all(d['with_forma'] == [1, 2, 3])
