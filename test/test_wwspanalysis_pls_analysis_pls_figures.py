from wwspanalysis.PLS_analysis.pls_figures import RegressionAnalysisPlot, RegressionAnalysisPlotDetailed
import numpy as np
import seaborn as sns

def test_regression_analysis_plot():
    y = np.array([1, 2, 3, 4, 5])
    y_pred = y

    fig = RegressionAnalysisPlot(y, y_pred)

    assert fig.fig is not None
    assert sns.lineplot is not None

def test_regression_analysis_plot_detailed():
    y = np.array([1, 2, 3, 4, 5])
    y_pred = y

    fig = RegressionAnalysisPlotDetailed(y, y_pred)

    assert fig.fig is not None
    assert sns.lineplot is not None

    assert fig.fig is not None
    assert fig.ax is not None
    assert len(fig.ax)==3