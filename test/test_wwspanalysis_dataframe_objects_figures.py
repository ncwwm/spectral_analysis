from wwspanalysis.dataframe_objects.figures import HeatMap, HistPlot
import seaborn as sns
import pandas as pd

def test_Histplot1():
    data = [0, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5]
    histplot = HistPlot(data)
    assert histplot.fig is not None
    assert sns.histplot is not None

def test_Histplot2():
    data = {'a': [0, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5]}
    df = pd.DataFrame(data)
    histplot = HistPlot(df, column_name='a')
    assert histplot.fig is not None
    assert sns.histplot is not None

def test_Heatmap():
    data = pd.DataFrame({'a': [0, 1, 2, 3, 4, 5], 'b': [2, 5, 6, 3, 4, 5]})
    heatmap = HeatMap(data)

    assert heatmap.fig is not None
    assert sns.heatmap is not None

def test_MeanSpectralDfplot():
    pass