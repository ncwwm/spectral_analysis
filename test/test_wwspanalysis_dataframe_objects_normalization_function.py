import numpy as np
from wwspanalysis.dataframe_objects.normalization_function import normalize_array, normalize_df
import pandas as pd


def test_normalize_array():

    arr = np.array([1, 2, 3, 4, 5])

    assert np.allclose(normalize_array(arr, method='mean'), np.array([-0.5, -0.25, 0, 0.25, 0.5]), rtol=1e-5)
    assert np.allclose(normalize_array(arr, method='zero_one'), np.array([0, 0.25, 0.5, 0.75, 1]), rtol=1e-5)

    std = np.std(arr)
    assert np.allclose(normalize_array(arr, method='std'), np.array([-2/std, -1/std, 0/std, 1/std, 2/std]), rtol=1e-5)

    assert np.allclose(normalize_array(arr), np.array([-2/std, -1/std, 0/std, 1/std, 2/std]), rtol=1e-5)

    # test for invalid input method
    try:
        normalize_array(arr, method='invalid')
    except Exception as e:
        assert str(e) == 'This normalization method is not available'


def test_normalize_df():

    df = pd.DataFrame({'a': [1.0, 2.0, 3.0], 'b': [4.0, 5.0, 6.0], 'c': [7.0, 8.0, 9.0]})

    # test for line normalization with mean method
    norm_df = normalize_df(df, method='mean', dimension='line')
    expected_df = pd.DataFrame({'a': [-0.5, -0.5, -0.5], 'b': [0.0, 0.0, 0.0], 'c': [0.5, 0.5, 0.5]})
    pd.testing.assert_frame_equal(norm_df, expected_df)

    # test for column normalization with zero_one method
    norm_df = normalize_df(df, method='zero_one', dimension='col')
    expected_df = pd.DataFrame({'a': [0.0, 0.5, 1.0], 'b': [0.0, 0.5, 1.0], 'c': [0.0, 0.5, 1.0]})
    pd.testing.assert_frame_equal(norm_df, expected_df)

    # test for invalid normalization method
    try:
        normalize_df(df, method='invalid', dimension='line')
    except Exception as e:
        assert str(e) == 'This normalization method is not available'

    # test for invalid normalization dimension
    try:
        normalize_df(df, method='std', dimension='invalid')
    except Exception as e:
        assert str(e) == 'The normalization dimension is either \'line\' or \'col\''